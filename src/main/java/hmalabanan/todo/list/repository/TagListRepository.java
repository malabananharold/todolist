package hmalabanan.todo.list.repository;

import hmalabanan.todo.list.model.TagList;
import hmalabanan.todo.list.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by anathema on 22/05/2016.
 */
public interface TagListRepository extends JpaRepository<TagList,Long> {

    @Query("SELECT tags FROM TagList tags WHERE tags.user=:userId")
    List<TagList> getTagsByUser(@Param("userId")User userId);
}
