package hmalabanan.todo.list.repository;

import hmalabanan.todo.list.model.TagList;
import hmalabanan.todo.list.model.Todo;

import hmalabanan.todo.list.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by anathema on 22/05/2016.
 */
public interface TodoRepository extends JpaRepository<Todo,Long>{

    @Query("SELECT todo FROM Todo todo WHERE todo.tagList=:tagList")
    List<Todo> getTodoByTagList(@Param("tagList")TagList tagList);

    @Query("SELECT todo FROM Todo todo WHERE todo.tagList=:tagList AND todo.tagList.user=:user")
    List<Todo> getTodoByTagListAndUser(@Param("tagList")TagList tagList,@Param("user")User user);

    @Query("SELECT todo FROM Todo todo WHERE todo.tagList.user=:user")
    List<Todo> getAllTodoByUser(@Param("user")User user);
}
