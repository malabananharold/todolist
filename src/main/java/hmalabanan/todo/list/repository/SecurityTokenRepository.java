package hmalabanan.todo.list.repository;

import hmalabanan.todo.list.model.SecurityToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by anathema on 22/05/2016.
 */
public interface SecurityTokenRepository extends JpaRepository<SecurityToken,Long>{
    @Query("SELECT secToken FROM SecurityToken secToken WHERE secToken.token=:token")
    SecurityToken findSecurityTokenByToken(@Param("token")String token);
}
