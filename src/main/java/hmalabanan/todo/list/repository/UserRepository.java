package hmalabanan.todo.list.repository;

import hmalabanan.todo.list.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by anathema on 22/05/2016.
 */
public interface UserRepository extends JpaRepository<User,Long> {
    User findUserByUserName(String username);

    @Query("SELECT user from User user where user.userName=:userName AND user.password=:password")
    User findUserByUserNameAndPassword(@Param("userName")String userName,@Param("password")String password);
}
