package hmalabanan.todo.list.service.impl;

import hmalabanan.todo.list.exceptions.TodoListUserNotFoundException;
import hmalabanan.todo.list.model.User;
import hmalabanan.todo.list.repository.UserRepository;
import hmalabanan.todo.list.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by anathema on 22/05/2016.
 */
@Service
public class UserServiceImpl implements UserService{

    @Autowired
    private UserRepository userRepository;

    @Override
    public User findUserByUserName(String userName) throws TodoListUserNotFoundException {
        User user = null;
        user = userRepository.findUserByUserName(userName);
        if(user == null){
            throw new TodoListUserNotFoundException("User with {} does not exists");
        }
        return user;
    }

    @Override
    public String validateLogin(String userName, String password) {
        User user = userRepository.findUserByUserNameAndPassword(userName,password);
        if(user != null && user.getPassword().equals(password)){

        }
        return null;
    }
}
