package hmalabanan.todo.list.service;

import hmalabanan.todo.list.exceptions.TodoListUserNotFoundException;
import hmalabanan.todo.list.model.User;

/**
 * Created by anathema on 22/05/2016.
 */
public interface UserService {
    User findUserByUserName(String userName) throws TodoListUserNotFoundException;
    String validateLogin(String userName,String password);
}
