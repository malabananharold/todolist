package hmalabanan.todo.list.enums;

/**
 * Created by anathema on 22/05/2016.
 */
public enum TodoListConstants {

    TODO_LIST("todolist"),
    WEB_INF_PREFIX("/WEB-INF/pages/"),
    HTML_SUFFIX(".html"),
    DISPATCHER("dispatcher"),
    ROOT_MAPPING("/"),
    DB_DRIVER_CLASS("db.DriverClass"),
    DB_USER_NAME("db.userName"),
    DB_PASSWORD("db.password"),
    DB_JDBC_URL("db.jdbcUrl"),
    MODEL_ENTITY_PACKAGES("hmlabanan.todo.list.model");


    String constantValue;

    TodoListConstants(String constantValue){
        this.constantValue = constantValue;
    }

    public String getConstantValue() {
        return constantValue;
    }
}
