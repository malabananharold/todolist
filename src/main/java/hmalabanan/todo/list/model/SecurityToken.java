package hmalabanan.todo.list.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by anathema on 22/05/2016.
 */
@Entity
@Table(name="security_token")
public class SecurityToken {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne
    @JoinColumn(name="user_id")
    private User user;

    @Column
    private String token;

    @Column(name="time_stamp")
    private Date timeStamp;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
}
