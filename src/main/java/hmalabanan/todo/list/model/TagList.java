package hmalabanan.todo.list.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.Set;

/**
 * Created by anathema on 22/05/2016.
 */
@Entity
@Table(name = "tag_list")
public class TagList implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(nullable = false)
    private String tags;

    @OneToOne
    @JoinColumn(nullable = false,name = "user_id")
    private User user;

    @Column(nullable = false,name="time_stamp")
    private Date timeStamp;

//    @OneToMany(mappedBy = "tag_list",fetch = FetchType.LAZY)
//    private Set<Todo> todoSet;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Date getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Date timeStamp) {
        this.timeStamp = timeStamp;
    }
//
//    public Set<Todo> getTodoSet() {
//        return todoSet;
//    }
//
//    public void setTodoSet(Set<Todo> todoSet) {
//        this.todoSet = todoSet;
//    }
}
