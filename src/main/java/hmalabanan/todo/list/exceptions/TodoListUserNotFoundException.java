package hmalabanan.todo.list.exceptions;

/**
 * Created by anathema on 22/05/2016.
 */
public class TodoListUserNotFoundException extends Exception {

    public TodoListUserNotFoundException(Exception e){
        super(e);
    }

    public TodoListUserNotFoundException(String message){
        super(message);
    }
}
