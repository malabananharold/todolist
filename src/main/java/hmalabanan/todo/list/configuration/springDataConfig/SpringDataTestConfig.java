package hmalabanan.todo.list.configuration.springDataConfig;

import com.jolbox.bonecp.BoneCPDataSource;
import hmalabanan.todo.list.enums.TodoListConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.hibernate4.HibernateExceptionTranslator;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;

/**
 * Created by anathema on 22/05/2016.
 */
@Configuration
@EnableJpaRepositories(basePackages = "hmalabanan.todo.list",entityManagerFactoryRef = "entityManager",transactionManagerRef = "transactionManager")
@EnableTransactionManagement
@PropertySource("classpath:application_test.properties")
public class SpringDataTestConfig {
    @Autowired
    private Environment environment;

    @Bean
    public DataSource dataSource(){
        BoneCPDataSource boneCPDataSource = new BoneCPDataSource();
        boneCPDataSource.setDriverClass(environment.getProperty(TodoListConstants.DB_DRIVER_CLASS.getConstantValue()));
        boneCPDataSource.setUsername(environment.getProperty(TodoListConstants.DB_USER_NAME.getConstantValue()));
        boneCPDataSource.setPassword(environment.getProperty(TodoListConstants.DB_PASSWORD.getConstantValue()));
        boneCPDataSource.setJdbcUrl(environment.getProperty(TodoListConstants.DB_JDBC_URL.getConstantValue()));
        return boneCPDataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManager(){
        LocalContainerEntityManagerFactoryBean localContainerEntityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        localContainerEntityManagerFactoryBean.setDataSource(dataSource());
        localContainerEntityManagerFactoryBean.setPersistenceUnitName(TodoListConstants.TODO_LIST.getConstantValue());
        localContainerEntityManagerFactoryBean.setPackagesToScan(TodoListConstants.MODEL_ENTITY_PACKAGES.getConstantValue());
        return localContainerEntityManagerFactoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(){
        JpaTransactionManager jpaTransactionManager = new JpaTransactionManager();
        jpaTransactionManager.setEntityManagerFactory(entityManager().getObject());
        return jpaTransactionManager;
    }

    @Bean
    public HibernateExceptionTranslator hibernateExceptionTranslator(){
        return new HibernateExceptionTranslator();
    }
}
