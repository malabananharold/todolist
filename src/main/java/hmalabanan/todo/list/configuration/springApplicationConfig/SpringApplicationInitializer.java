package hmalabanan.todo.list.configuration.springApplicationConfig;

import hmalabanan.todo.list.enums.TodoListConstants;
import org.apache.velocity.runtime.resource.loader.ResourceLoader;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.ViewResolverRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.velocity.VelocityConfigurer;
import org.springframework.web.servlet.view.velocity.VelocityView;
import org.springframework.web.servlet.view.velocity.VelocityViewResolver;

/**
 * Created by anathema on 22/05/2016.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = "hmalabanan.todo.list")
public class SpringApplicationInitializer extends WebMvcConfigurerAdapter{

    @Bean
    public VelocityConfigurer velocityConfig() {
        VelocityConfigurer velocityConfigurer = new VelocityConfigurer();
        velocityConfigurer.setResourceLoaderPath(TodoListConstants.WEB_INF_PREFIX.getConstantValue());
        return velocityConfigurer;
    }

    @Override
    public void configureViewResolvers(ViewResolverRegistry registry) {
        VelocityViewResolver viewResolver = new VelocityViewResolver();
        viewResolver.setViewClass(VelocityView.class);
        viewResolver.setCache(true);
        viewResolver.setPrefix("");
        viewResolver.setSuffix(TodoListConstants.HTML_SUFFIX.getConstantValue());
        viewResolver.setExposeSpringMacroHelpers(true);
        registry.viewResolver(viewResolver);
    }

//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        registry.addResourceHandler("pages/**").addResourceLocations(TodoListConstants.WEB_INF_PREFIX.getConstantValue());
//    }
}
