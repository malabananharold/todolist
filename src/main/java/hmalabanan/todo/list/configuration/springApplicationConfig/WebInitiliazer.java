package hmalabanan.todo.list.configuration.springApplicationConfig;

import hmalabanan.todo.list.configuration.springApplicationConfig.SpringApplicationInitializer;
import hmalabanan.todo.list.enums.TodoListConstants;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

/**
 * Created by anathema on 22/05/2016.
 */
public class WebInitiliazer implements WebApplicationInitializer {
    @Override
    public void onStartup(ServletContext servletContext) throws ServletException {
        AnnotationConfigWebApplicationContext annotationConfigWebApplicationContext = new AnnotationConfigWebApplicationContext();
        annotationConfigWebApplicationContext.register(SpringApplicationInitializer.class);
        annotationConfigWebApplicationContext.setServletContext(servletContext);

        ServletRegistration.Dynamic servlet = servletContext.addServlet(TodoListConstants.DISPATCHER.getConstantValue(), new DispatcherServlet(annotationConfigWebApplicationContext));
        servlet.setLoadOnStartup(1);
        servlet.addMapping(TodoListConstants.ROOT_MAPPING.getConstantValue());
    }
}
