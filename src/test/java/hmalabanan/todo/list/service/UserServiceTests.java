package hmalabanan.todo.list.service;

import hmalabanan.todo.list.exceptions.TodoListUserNotFoundException;
import hmalabanan.todo.list.model.User;
import hmalabanan.todo.list.repository.UserRepository;
import hmalabanan.todo.list.service.impl.UserServiceImpl;
import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by anathema on 22/05/2016.
 */
@RunWith(PowerMockRunner.class)
public class UserServiceTests {

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Test
    public void searchUserMustNotNull() throws TodoListUserNotFoundException {
        User user = PowerMockito.mock(User.class);
        PowerMockito.when(userRepository.findUserByUserName(Matchers.anyString())).thenReturn(user);
        assertThat(userService.findUserByUserName("harold")).isNotEqualTo(null);
    }

    @Test(expected = TodoListUserNotFoundException.class)
    public void searchUserMustBeNull() throws TodoListUserNotFoundException {
        PowerMockito.when(userRepository.findUserByUserName(Matchers.anyString())).thenReturn(null);
        assertThat(userService.findUserByUserName("kenneth")).isEqualTo(null);
    }

    @Test
    public void validateUserLogin() throws TodoListUserNotFoundException {
        User user = PowerMockito.mock(User.class);
        String sampleToken = PowerMockito.mock(String.class);
        PowerMockito.when(userRepository.findUserByUserNameAndPassword(Matchers.anyString(),Matchers.anyString())).thenReturn(user);
        PowerMockito.when(user.getPassword()).thenReturn(sampleToken);
        User testUser = userService.validateLogin("harold","kenneth");
        String token = testUser.getPassword();
        assertThat(token).isNotEqualTo(null);
    }
}
