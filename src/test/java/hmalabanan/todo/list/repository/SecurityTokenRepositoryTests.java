package hmalabanan.todo.list.repository;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import hmalabanan.todo.list.configuration.springDataConfig.SpringDataConfig;
import hmalabanan.todo.list.configuration.springDataConfig.SpringDataTestConfig;
import hmalabanan.todo.list.model.SecurityToken;
import hmalabanan.todo.list.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by anathema on 22/05/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringDataTestConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class SecurityTokenRepositoryTests {

    @Autowired
    private SecurityTokenRepository securityTokenRepository;

    @Test
    @DatabaseSetup(value = "classpath:hmalabanan/todo/list/securityTokenData.xml")
    @DatabaseTearDown(value = "classpath:hmalabanan/todo/list/securityTokenData.xml",type = DatabaseOperation.DELETE_ALL)
    public void ifTokenExists(){
        SecurityToken securityToken = securityTokenRepository.findSecurityTokenByToken("sampleToken123");
        User user = securityToken.getUser();
        assertThat(user.getUserName()).isEqualTo("kenneth");
        assertThat(securityToken.getToken()).isEqualTo("sampleToken123");
    }

    @Test
    @DatabaseSetup(value = "classpath:hmalabanan/todo/list/securityTokenData.xml")
    @DatabaseTearDown(value = "classpath:hmalabanan/todo/list/securityTokenData.xml",type = DatabaseOperation.DELETE_ALL)
    public void ifTokenDoesNotExists(){
        SecurityToken securityToken = securityTokenRepository.findSecurityTokenByToken("sampleToken");
        assertThat(securityToken).isEqualTo(null);
    }
}
