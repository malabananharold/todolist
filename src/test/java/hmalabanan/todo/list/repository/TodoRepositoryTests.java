package hmalabanan.todo.list.repository;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import hmalabanan.todo.list.configuration.springDataConfig.SpringDataTestConfig;
import hmalabanan.todo.list.model.TagList;
import hmalabanan.todo.list.model.Todo;
import hmalabanan.todo.list.model.User;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.atIndex;

/**
 * Created by anathema on 22/05/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringDataTestConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class TodoRepositoryTests {

    @Autowired
    private TodoRepository todoRepository;

    private static TagList tagList;
    private static User user;

    @Before
    public void createTagListclass(){
        tagList = new TagList();
        user = new User();
        user.setId(1);
        user.setUserName("kenneth");
        user.setPassword("123456");
        user.setDateCreated(new Date());

        tagList.setId(1);
        tagList.setUser(user);
        tagList.setTags("Personal");
        tagList.setTimeStamp(new Date());
    }

    @Test
    @DatabaseSetup(value = "classpath:hmalabanan/todo/list/todoData.xml")
    @DatabaseTearDown(value = "classpath:hmalabanan/todo/list/todoData.xml",type = DatabaseOperation.DELETE_ALL)
    public void getAllTodoByTagList(){
        List<Todo> todoList = todoRepository.getTodoByTagList(tagList);
        assertThat(todoList.size()).isGreaterThan(0);
    }


    @Test
    @DatabaseSetup(value = "classpath:hmalabanan/todo/list/todoData.xml")
    @DatabaseTearDown(value = "classpath:hmalabanan/todo/list/todoData.xml",type = DatabaseOperation.DELETE_ALL)
    public void getAllTodoByTagListAndUser(){
        List<Todo> todoList = todoRepository.getTodoByTagListAndUser(tagList, user);
        assertThat(todoList.size()).isGreaterThan(0);
    }

    @Test
    @DatabaseSetup(value = "classpath:hmalabanan/todo/list/todoData.xml")
    @DatabaseTearDown(value = "classpath:hmalabanan/todo/list/todoData.xml",type = DatabaseOperation.DELETE_ALL)
    public void getAllTodoByUser(){
        List<Todo> todoList = todoRepository.getAllTodoByUser(user);
        assertThat(todoList.size()).isGreaterThan(0);
    }
}
