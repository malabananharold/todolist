package hmalabanan.todo.list.repository;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import hmalabanan.todo.list.configuration.springApplicationConfig.SpringApplicationInitializer;
import hmalabanan.todo.list.configuration.springDataConfig.SpringDataConfig;
import hmalabanan.todo.list.configuration.springDataConfig.SpringDataTestConfig;
import hmalabanan.todo.list.model.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by anathema on 22/05/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {SpringDataTestConfig.class})
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class})
public class UserRepositoryTests {

    @Autowired
    UserRepository userRepository;

    @Test
    @DatabaseSetup(value="classpath:hmalabanan/todo/list/userData.xml")
    @DatabaseTearDown(value="classpath:hmalabanan/todo/list/userData.xml", type=DatabaseOperation.DELETE_ALL)
    public void findUserByUserName(){
        User user = userRepository.findUserByUserName("harold");
        assertThat(user.getUserName()).isEqualTo("harold");
    }

    @Test
    @DatabaseSetup(value="classpath:hmalabanan/todo/list/userData.xml")
    @DatabaseTearDown(value="classpath:hmalabanan/todo/list/userData.xml", type=DatabaseOperation.DELETE_ALL)
    public void nullIfUserNotExists(){
        User user = userRepository.findUserByUserName("sampleUser");
        assertThat(user).isEqualTo(null);
    }

    @Test
    @DatabaseSetup(value="classpath:hmalabanan/todo/list/userData.xml")
    @DatabaseTearDown(value="classpath:hmalabanan/todo/list/userData.xml", type=DatabaseOperation.DELETE_ALL)
    public void findUserByUserNameAndPassword(){
        User user = userRepository.findUserByUserNameAndPassword("harold","123456");
        assertThat(user.getUserName()).isEqualTo("harold");
    }
}
